-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 06 Jun 2020 pada 19.52
-- Versi server: 10.1.30-MariaDB
-- Versi PHP: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rentalmusik`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `alat_musik`
--

CREATE TABLE `alat_musik` (
  `kode_alat` int(11) NOT NULL,
  `foto_alat` varchar(1000) NOT NULL,
  `nama_alat` varchar(100) NOT NULL,
  `Merk_alat` int(11) NOT NULL,
  `tahun_beli` int(11) NOT NULL,
  `jumlah_item` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `alat_musik`
--

INSERT INTO `alat_musik` (`kode_alat`, `foto_alat`, `nama_alat`, `Merk_alat`, `tahun_beli`, `jumlah_item`) VALUES
(1, '8 pro.jfif', 'gitar', 0, 2020, 50),
(2, 'IMG_4923.jpg', 'drum', 0, 2016, 30);

-- --------------------------------------------------------

--
-- Struktur dari tabel `artikel`
--

CREATE TABLE `artikel` (
  `id_artikel` int(11) NOT NULL,
  `judul_artikel` text NOT NULL,
  `konten_artikel` text NOT NULL,
  `tanggal_terbit` date NOT NULL,
  `Penerbit` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `artikel`
--

INSERT INTO `artikel` (`id_artikel`, `judul_artikel`, `konten_artikel`, `tanggal_terbit`, `Penerbit`) VALUES
(1, 'post satu', 'haloooo', '2020-04-07', 'daffa');

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_sewa`
--

CREATE TABLE `data_sewa` (
  `kode_sewa` int(11) NOT NULL,
  `penyewa` varchar(100) NOT NULL,
  `kontak_penyewa` int(13) NOT NULL,
  `alamat_penyewa` varchar(10000) NOT NULL,
  `paket_sewa` varchar(100) NOT NULL,
  `almus_sewa` varchar(10000) NOT NULL,
  `tambahan` varchar(10000) NOT NULL,
  `status_sewa` enum('Sewa','Kembali') NOT NULL,
  `tanggal_mulai` date NOT NULL,
  `tanggal_selesai` date NOT NULL,
  `tanggal_kembali` date NOT NULL,
  `catatan` varchar(10000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `data_sewa`
--

INSERT INTO `data_sewa` (`kode_sewa`, `penyewa`, `kontak_penyewa`, `alamat_penyewa`, `paket_sewa`, `almus_sewa`, `tambahan`, `status_sewa`, `tanggal_mulai`, `tanggal_selesai`, `tanggal_kembali`, `catatan`) VALUES
(1, 'edo', 2147483647, 'jaksel', '1', 'gitar', '-', 'Kembali', '2020-06-25', '2020-06-26', '2020-06-27', 'telAT 1 HARI');

-- --------------------------------------------------------

--
-- Struktur dari tabel `paket_sewa`
--

CREATE TABLE `paket_sewa` (
  `kode_paket` int(11) NOT NULL,
  `nama_paket` varchar(1000) NOT NULL,
  `alat_sewa` varchar(1000) NOT NULL,
  `catatan` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `sound_system`
--

CREATE TABLE `sound_system` (
  `kode_gambar` int(11) NOT NULL,
  `foto_alat` int(11) NOT NULL,
  `nama_alat` varchar(1000) NOT NULL,
  `merk_alat` varchar(1000) NOT NULL,
  `tahun_beli` int(4) NOT NULL,
  `jumlah_item` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `sound_system`
--

INSERT INTO `sound_system` (`kode_gambar`, `foto_alat`, `nama_alat`, `merk_alat`, `tahun_beli`, `jumlah_item`) VALUES
(4, 0, 'Speaker', 'Dzildian', 2017, 100),
(5, 0, 'Speaker', 'Yamaha', 2017, 115),
(6, 0, 'Microphone', 'Kenwood', 2019, 50),
(7, 0, 'panci', 'cosmos', 2015, 12);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `alat_musik`
--
ALTER TABLE `alat_musik`
  ADD PRIMARY KEY (`kode_alat`);

--
-- Indeks untuk tabel `artikel`
--
ALTER TABLE `artikel`
  ADD PRIMARY KEY (`id_artikel`);

--
-- Indeks untuk tabel `data_sewa`
--
ALTER TABLE `data_sewa`
  ADD PRIMARY KEY (`kode_sewa`);

--
-- Indeks untuk tabel `paket_sewa`
--
ALTER TABLE `paket_sewa`
  ADD PRIMARY KEY (`kode_paket`);

--
-- Indeks untuk tabel `sound_system`
--
ALTER TABLE `sound_system`
  ADD PRIMARY KEY (`kode_gambar`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `alat_musik`
--
ALTER TABLE `alat_musik`
  MODIFY `kode_alat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `artikel`
--
ALTER TABLE `artikel`
  MODIFY `id_artikel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `data_sewa`
--
ALTER TABLE `data_sewa`
  MODIFY `kode_sewa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `paket_sewa`
--
ALTER TABLE `paket_sewa`
  MODIFY `kode_paket` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `sound_system`
--
ALTER TABLE `sound_system`
  MODIFY `kode_gambar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
