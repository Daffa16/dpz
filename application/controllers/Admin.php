<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

    public function __construct(){
        parent :: __construct();
		$this->load->model('M_renmus');
		$this->load->helper(array('form', 'url'));
    }

	public function index(){
        $this->load->view('admin/header');
        $this->load->view('admin/middle');
        $this->load->view('admin/footer');
    }

    public function tulis_informasi(){
        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('title', 'Judul', 'required');
		$this->form_validation->set_rules('sender', 'Penerbit', 'required');
		$this->form_validation->set_rules('terbit', 'Tanggal Terbit', 'required');
		$this->form_validation->set_rules('content', 'Konten', 'required');

        if($this->form_validation->run() === FALSE){
            $this->load->view('admin/header');
            $this->load->view('admin/tulis_info');
            $this->load->view('admin/footer');
        }else{
            if($this->input->post('input',true)):
                $this->db->insert('artikel',
                                 array('judul_artikel'=>$this->input->post('title',true),
                                        'konten_artikel'=>$this->input->post('content',true),
                                        'tanggal_terbit'=>$this->input->post('terbit',true),
                                        'penerbit'=>$this->input->post('sender',true)), 
                                array('id_artikel'=>$kode));
                                redirect('admin/tulis_informasi');
        endif;}
    }

    public function informasi(){
        $data['artikel'] = $this->db->query("select * from artikel order by id_artikel")->result();

        $this->load->view('admin/header');
        $this->load->view('admin/info', $data);
        $this->load->view('admin/footer');
    }

    public function sound_system(){
        $data['sound_system'] = $this->db->query("select * from sound_system order by kode_gambar")->result();

        $this->load->view('admin/header');
        $this->load->view('admin/sound', $data);
        $this->load->view('admin/footer');
    }

    public function paket(){
        $data['paket_sewa'] = $this->db->query("select * from paket_sewa order by kode_paket")->result();
        $this->load->view('admin/header');
        $this->load->view('admin/paket', $data);
        $this->load->view('admin/footer');
    }

    public function alat_musik(){
        $data['alat_musik'] = $this->db->query("select * from alat_musik order by kode_alat")->result();
        
        $this->load->view('admin/header');
        $this->load->view('admin/almus', $data);
        $this->load->view('admin/footer');
    }

    public function sewa(){
        $data['data_sewa'] = $this->db->query("select * from data_sewa order by kode_sewa")->result();

        $this->load->view('admin/header');
        $this->load->view('admin/rent', $data);
        $this->load->view('admin/footer');
    }

    public function selesai(){
        $this->load->view('admin/header');
        $this->load->view('admin/finish');
        $this->load->view('admin/footer');
    }

    public function berita($kode){
        $where = array('id_artikel' => $kode);
        $data['artikel'] = $this->M_renmus->edit_data($where, 'artikel')->result();
            
        $this->load->view('admin/header');
        $this->load->view('admin/berita', $data);
        $this->load->view('admin/footer');
    }

    public function nambah_almus(){
        $this->load->helper('form');
        $this->load->library('form_validation');

       
        $this->form_validation->set_rules('pic_mus', 'Gambar', 'required');
        $this->form_validation->set_rules('nama', 'Nama Alat', 'required');
        $this->form_validation->set_rules('merk_almus', 'Merk Alat', 'required');
        $this->form_validation->set_rules('tahun', 'Tahun Beli', 'required');
        $this->form_validation->set_rules('it_mus', 'Jumlah Item', 'required');
        
        if($this->form_validation->run() === FALSE){
        $this->load->view('admin/header');
        $this->load->view('admin/tambah_alat');
        $this->load->view('admin/footer');
        }else{
            if($this->input->post('input',true)):
                $this->db->insert('sound_system',
                            array('foto_alat'=>$this->input->post('pic_mus',true),
                                'nama_alat'=>$this->input->post('nama',true),
                                'merk_alat'=>$this->input->post('merk_almus',true),
                                'tahun_beli'=>$this->input->post('tahun',true),
                                'jumlah_item'=>$this->input->post('it_mus',true)), 
                            array('kode_gambar' => $id));
                            redirect('admin/sound_system');
        endif;}
    }

    public function update_sound($id=NULL){
            if($this->input->post('input',true)):
                $this->db->update('sound_system',
                            array('foto_alat'=>$this->input->post('pic_mus',true),
                            'nama_alat'=>$this->input->post('nama',true),
                            'merk_alat'=>$this->input->post('merk_almus',true),
                            'tahun_beli'=>$this->input->post('tahun',true),
                            'jumlah_item'=>$this->input->post('it_mus',true)), 
                            array('kode_gambar' => $id));
                            redirect('admin/sound_system');
        endif;
        $this->data['sound'] = $this->db->get_where('sound_system', 
            array('kode_gambar'=>$id))->row();
        $this->load->view('admin/header');
        $this->load->view('admin/edit_sound', $this->data);
        $this->load->view('admin/footer');
    }

    Public function delete_sound($id=NULL){
		$this->db->delete( 'sound_system',
						array('kode_gambar'=>$id));
		redirect('admin/sound_system');
	}

	public function nambah_musik(){
		$this->load->helper('form');
		$this->load->library('form_validation');
	
		// $this->form_validation->set_rules('pic_mus', 'Gambar', 'required');
		$this->form_validation->set_rules('nama', 'Nama Alat', 'required');
		$this->form_validation->set_rules('merk', 'Merk Alat', 'required');
		$this->form_validation->set_rules('tahun', 'Tahun Beli', 'required');
		$this->form_validation->set_rules('it_mus', 'Jumlah Item', 'required');

		if($this->form_validation->run() === FALSE){
			$this->load->view('admin/header');
			$this->load->view('admin/tambah_almus');
			$this->load->view('admin/footer');

		}else{
			// source input gambar
			$lokasi_image = $_FILES['gambar']['tmp_name'];
			if($_FILES['gambar']['name']==""){
			$cover = NULL;
			}else{
			$cover = rand(100,100000)."-".$_FILES['gambar']['name'];
			}
			$temp="./uploads/$cover";
			move_uploaded_file($lokasi_image,$temp);
			// end source input gambar

				if($this->input->post('input',true)):
					$this->db->insert('alat_musik',
					array(
						'foto_alat'=> $cover,
						'nama_alat'=>$this->input->post('nama',true),
						'Merk_alat'=>$this->input->post('merk',true),
						'tahun_beli'=>$this->input->post('tahun',true),
						'jumlah_item'=>$this->input->post('it_mus',true)
					));
					// array('kode_gambar' => $id));
					redirect('admin/alat_musik');
			endif;
		}
    }

    public function update_alat($id=NULL){
        if($this->input->post('input',true)):
            $this->db->update('alat_musik',
                        array('foto_alat'=>$this->input->post('pic_mus',true),
                        'nama_alat'=>$this->input->post('nama',true),
                        'Merk_alat'=>$this->input->post('merk',true),
                        'tahun_beli'=>$this->input->post('tahun',true),
                        'jumlah_item'=>$this->input->post('it_mus',true)), 
                        array('kode_alat' => $id));
                        redirect('admin/alat_musik');
    endif;
    $this->data['sound'] = $this->db->get_where('alat_musik', 
        array('kode_alat'=>$id))->row();
    $this->load->view('admin/header');
    $this->load->view('admin/edit_alat', $this->data);
    $this->load->view('admin/footer');
    }

    Public function delete_alat($id=NULL){
		$this->db->delete( 'alat_musik',
						array('kode_alat'=>$id));
		redirect('admin/alat_musik');
    }

    public function nambah_paket(){
        $this->load->helper('form');
        $this->load->library('form_validation');
    
       
        $this->form_validation->set_rules('nampak', 'Nama', 'required');
        $this->form_validation->set_rules('alat', 'Alat', 'required');
        
        if($this->form_validation->run() === FALSE){
        $this->load->view('admin/header');
        $this->load->view('admin/nambah_paket');
        $this->load->view('admin/footer');
        }else{
            if($this->input->post('input',true)):
                $this->db->insert('paket_sewa',
                            array('nama_paket'=>$this->input->post('nampak',true),
                                'alat_sewa'=>$this->input->post('alat',true),
                                'catatan'=>$this->input->post('cat',true)), 
                            array('kode_paket' => $id));
                            redirect('admin/paket');
        endif;}
        }
        
        public function update_paket($id=NULL){
            if($this->input->post('input',true)):
                $this->db->update('paket_sewa',
                            array('nama_paket'=>$this->input->post('nampak',true),
                            'alat_sewa'=>$this->input->post('alat',true),
                            'catatan'=>$this->input->post('cat',true)), 
                            array('kode_paket' => $id));
                            redirect('admin/paket');
        endif;
        $this->data['sound'] = $this->db->get_where('paket_sewa', 
            array('kode_paket'=>$id))->row();
        $this->load->view('admin/header');
        $this->load->view('admin/edit_paket', $this->data);
        $this->load->view('admin/footer');
        }

        Public function delete_paket($id=NULL){
            $this->db->delete( 'paket_sewa',
                            array('kode_paket'=>$id));
            redirect('admin/paket');
        }

        public function nambah_sewa(){
            $this->load->helper('form');
            $this->load->library('form_validation');
        
           
            $this->form_validation->set_rules('name_rent', 'Nama Penyewa', 'required');
            $this->form_validation->set_rules('no_hp', 'Kontak Penyewa', 'required');
            $this->form_validation->set_rules('alamat', 'Alamat Penyewa', 'required');
            $this->form_validation->set_rules('paket', 'Paket Sewa', 'required');
            $this->form_validation->set_rules('status', 'Status Sewa', 'required');
            $this->form_validation->set_rules('date_rent', 'Tanggal Sewa', 'required');
            $this->form_validation->set_rules('date_finish', 'Tanggal Selesai', 'required');
           

            if($this->form_validation->run() === FALSE){
            $this->load->view('admin/header');
            $this->load->view('admin/tambah_sewa');
            $this->load->view('admin/footer');
            }else{
                if($this->input->post('input',true)):
                    $this->db->insert('data_sewa',
                                array('penyewa'=>$this->input->post('name_rent',true),
                                    'kontak_penyewa'=>$this->input->post('no_hp',true),
                                    'alamat_penyewa'=>$this->input->post('alamat',true),
                                    'paket_sewa'=>$this->input->post('paket',true),
                                    'almus_sewa'=>$this->input->post('almus',true),
                                    'tambahan'=>$this->input->post('tambah',true),
                                    'status_sewa'=>$this->input->post('status',true),
                                    'tanggal_mulai'=>$this->input->post('date_rent',true),
                                    'tanggal_selesai'=>$this->input->post('date_finish',true),
                                    'tanggal_kembali'=>$this->input->post('date_back',true),
                                    'catatan'=>$this->input->post('catatan',true)),     
                                array('kode_sewa' => $id));
                                redirect('admin/sewa');
            endif;}
            }
        
        public function view_sewa($id){
            $data['sound'] = $this->db->get_where('data_sewa', 
            array('kode_sewa'=>$id))->row();

            $this->load->view('admin/header');
            $this->load->view('admin/view_sewa', $data);
            $this->load->view('admin/footer');
        }

        public function update_sewa($id=NULL){
            if($this->input->post('input',true)):
                $this->db->update('data_sewa',
                            array('kontak_penyewa'=>$this->input->post('no_hp',true),
                            'alamat_penyewa'=>$this->input->post('alamat',true),
                            'tambahan'=>$this->input->post('tambah',true),
                            'status_sewa'=>$this->input->post('status',true),
                            'tanggal_kembali'=>$this->input->post('date_back',true),
                            'catatan'=>$this->input->post('catatan',true)),
                            array('kode_sewa' => $id));
                            redirect('admin/sewa');
        endif;
        $this->data['sound'] = $this->db->get_where('data_sewa', 
            array('kode_sewa'=>$id))->row();
        $this->load->view('admin/header');
        $this->load->view('admin/edit_sewa', $this->data);
        $this->load->view('admin/footer');
        }

        Public function delete_sewa($id=NULL){
            $this->db->delete( 'data_sewa',
                            array('kode_sewa'=>$id));
            redirect('admin/paket');
        }

    
}
