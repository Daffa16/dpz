<div>
    <h3>Tambah Alat Musik<h3>
</div>
    
<div>
    <?php if($sound): ?>
    <?php echo validation_errors(); ?>
    <?=form_open()?>

        <div>
            <label>Kode Sewa</label>
            <input type="text" name="name_rent" disabled value="<?=$sound->kode_sewa?>">
        </div>

        <div>
            <label>Nama Penyewa</label>
            <input type="text" name="name_rent" disabled value="<?=$sound->penyewa?>">
        </div>

        <div>
            <label>Nomor HP</label>
            <input type="text" name="no_hp" value="<?=$sound->kontak_penyewa?>">
        </div>

        <div>
            <label>Alamat</label>
            <input type="text" name="alamat" value="<?=$sound->alamat_penyewa?>">
        </div>

        <div>
            <label>Paket Sound System</label>
            <input type="text" name="paket" disabled value="<?=$sound->paket_sewa?>">
        </div>

        <div>
            <label>Alat Musik</label>
            <input type="text" name="almus" disabled value="<?=$sound->almus_sewa?>">
        </div>

        <div>
            <label>Tambahan</label>
            <input type="text" name="tambah" value="<?=$sound->tambahan?>">
        </div>

        <div>
            <label>Status Sewa</label>
            <input type="radio" value="Sewa" name="status"
            <?php if($sound->status_sewa =='Sewa'){
                    echo 'checked="checked"';
            } ?> />
                <label for="Sewa">Sedang Disewa</label>

            <input type="radio" value="Kembali" name="status"
            <?php if($sound->status_sewa =='Kembali'){
                    echo 'checked="checked"';
            } ?> />
                <label for="Kembali">Barang Kembali</label>
        </div>

        <div>
            <label>Tanggal Sewa</label>
            <input type="date" name="date_rent" disabled value="<?=$sound->tanggal_mulai?>">
        </div>

        <div>
            <label>Tanggal Selesai</label>
            <input type="date" name="date_finish" disabled value="<?=$sound->tanggal_selesai?>">
        </div>

        <div>
            <label>Tanggal Kembali</label>
            <input type="date" name="date_back" value="<?=$sound->tanggal_kembali?>">
        </div>

        <div>
            <label>Catatan</label>
            <input type="text" name="catatan" value="<?=$sound->catatan?>">
        </div>
    

        <div>
             <input type="submit" name="input" value="simpan">
        </div>
    <?=form_close()?>

    <?php else: ?>
        <Code>Product Not Found</code>
    <?php endif; ?>
</div>